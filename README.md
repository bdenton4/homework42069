# News for "Fundamentals of Digital Archeology"

### [Aug 29]
  * [Homework0](https://bitbucket.org/fdac/homework0/src/master/README.md)
  is due
  
  * [Lecture on data discovery](https://bitbucket.org/fdac/presentations/src/master/dd.pdf) finished

### [Aug 26]

  * [Lecture on data discovery](https://bitbucket.org/fdac/presentations/src/master/dd.pdf)

  * Will introduce Homework0 (to familiarize with basic python
  tools)

  * [The second task is due -- configuring ssh](https://bitbucket.org/fdac/students/src/master/Preliminary.md)

### [Aug 24]

  * [Magic about networks ports and so forth](https://bitbucket.org/fdac/presentations/raw/master/magic.pdf)

  * Analysis of self-description in .md files: fork fdac/students,
    and fdac/presentations then git clone in your docker terminal
    and play with students/TeamingAnalysis.ipynb and presentations/DataStructures.ipynb

  * Questions/issues with ssh/docker

  * Please finish [The second task -- configuring ssh](https://bitbucket.org/fdac/students/src/master/Preliminary.md)
  

### [Aug 23] Update
			   
  * The access to the servers is now available
	  * ssh to da2.eecs.utk.edu using windows (or mac/linux)
       instructions: if you are able to do that, then on your laptop
       browser please enter http://localhost:8888 to access your
       personal ipython notebook server. You can create python2,
       python3, and R notebooks. We will primarily use python3 and,
       later in the course, R
	   
    * Alternatively, if you'd prefer to run the ipython notebooks on your own
       laptop (not from the sever) please install docker
       infrastructure per https://www.docker.com/toolbox.  
         * The docker image used for the class is audris/ipython-pymongo16:latest
             * You will need to forward the port 8888 to port 8888 on that docker container on your laptop
             * You will also need to start 'ipython notebook --no-browser' in the container

### [Aug 22] Homework Due Aug 26 
  * [The second task -- configuring ssh](https://bitbucket.org/fdac/students/src/master/Preliminary.md)
  * Finish the first task (all done!!!)
    * Please make sure your USERNAME.md file is not empty and contains meaningful info. In particular, check if your .md file is in fdac/students and, if not, please submit a pull request.
    * I got all .md files but am still waiting for pull requests with .pub files
      from several of you, please submit your .pub files if you
      have not done so yet. I need it to add you to the class
      organization and to enable paswordless login.



## Aug 19
* The slides for the lecture today [tools.pdf](https://bitbucket.org/fdac/presentations/raw/master/tools.pdf)


## Aug 17

[The first tasks](https://bitbucket.org/fdac/students/src/master/Preliminary.md)

# Syllabus for "Fundamentals of Digital Archeology"
* **Course:** [COSCS-445/COSCS-545]
* ** MK405  2:30-3:20 MWF**
* **Instructor:** Audris Mockus, [audris@utk.edu](mailto:audris@utk.edu)
* **TA:** Tapajit Dey, [tdey2@vols.utk.edu](mailto:tdey2@vols.utk.edu) Office hours at MK620 2-4 pm on Tuesday
* Simple rules for getting help: 
    1. There are no stupid questions. However, it may be worth going over the following steps:
    1. Think of what the right answer may be.
    1. Search online: stack overflow, etc.
        * code snippets: On GH [gist.github.com](https://gist.github.com/) or, if anyone contributes, [for this class](https://bitbucket.org/snippets/fdac/)
        * answers to questions: [Stack Overflow](http://stackoverflow.com/)
    1. Look through [issues](https://bitbucket.org/fdac/news/issues)
    1. Post the question as an issue.
    1. Ask instructor: [email](mailto:audris@utk.edu) for 1-on-1 help, or
       to set up a time to meet 


## Objectives
The course will combine theoretical underpinning of big data with
intense practice. In particular, approaches to ethical concerns,
reproducibility of the results, absence of context, missing data,
and incorrect data will be both discussed and practiced by writing
programs to discover the data in the cloud, to retrieve it by
scraping the deep web, and by structuring, storing, and sampling it
in a way suitable for subsequent decision making.  At the end of the
course students will be able to discover, collect, and
clean digital traces, to use such traces to construct meaningful
measures, and to create tools that help with decision making.

## Expected Outcomes
Upon completion, students will be able to discover, gather, and analyze
digital traces, will learn how to avoid mistakes common in
the analysis of low-quality data, and will have produced a working
analytics application.

In particular, in addition to practicing critical thinking,
students will acquire the following skills:

*  Use Python and other tools to discover, retrieve, and process data.
  
*  Use data management techniques to store data locally and in the cloud.
  
*  Use data analysis methods to explore data and to make predictions.

## Course Description

A great volume of complex data is generated as a result of human
activities, including both work and play. To exploit that data for
decision making it is necessary to create software that discovers,
collects, and integrates the data.

Digital archeology relies on traces that are left over in the course
of ordinary activities, for example the logs generated by sensors in
mobile phones, the commits in version control systems, or the email
sent and the documents edited by a knowledge worker. Understanding
such traces is complicated in contrast to data collected using
traditional measurement approaches.

Traditional approaches rely on a highly controlled and well-designed
measurement system. In meteorology, for example, the temperature is
taken in specially designed and carefully selected locations to
avoid direct sunlight and to be at a fixed distance from the ground.
Such measurement can then be trusted to represent these controlled
conditions and the analysis of such data is, consequently, fairly
straightforward.

The measurements from geolocation or other sensors in mobile phones
are affected by numerous (yet not recorded) factors: was the phone
kept in the pocket, was it indoors or outside?  The devices are not
calibrated or may not work properly, so the corresponding
measurements would be inaccurate.  Locations (without mobile phones)
may not have any measurement, yet may be of the greatest interest.
This lack of context and inaccurate or missing data necessitates
fundamentally new approaches that rely on patterns of behavior to
correct the data, to fill in missing observations, and to elucidate
unrecorded context factors. These steps are needed to obtain
meaningful results from a subsequent analysis.

The course will cover basic principles and effective practices to
increase the integrity of the results obtained from voluminous but
highly unreliable sources.

*   Ethics: legal aspects, privacy, confidentiality, governance
   
*   Reproducibility: version control, ipython notebook
   
*   Fundamentals of big data analysis:
    extreme distributions, transformations, quantiles,
    sampling strategies, and
    logistic regression

*   The nature of digital traces:
    lack of context,
    missing values, and
    incorrect data

## Prerequisites

Students are expected to have basic programming skills, in
particular, be able to use regular expressions, programming concepts
such as variables, functions, loops, and data structures like lists
and dictionaries (for example, COSC 365)

Being familiar with version control systems (e.g., COSC 340), Python
(e.g., COSC 370), and introductory level probability (e.g., ECE 313)
and statistics, such as, random variables, distributions and
regression would be beneficial but is not expected. Everyone is
expected, however, to be willing and highly motivated to catch up in
the areas where they have gaps in the relevant skills.

All the assignments and projects for this class will use git and
Python. Knowledge of Python is not a prerequisite for this course,
provided you are comfortable learning on your own as needed. While
we have strived to make the programming component of this course
straightforward, we will not devote much time to teaching
programming, Python syntax, or any of the libraries and APIs.  You
should feel comfortable with:

1. How to look up Python syntax on Google and StackOverflow.
1. Basic programming concepts like functions, loops, arrays, dictionaries, strings, and if statements.
1. How to learn new libraries by reading documentation and reusing examples
1. Asking questions on StackOverflow or as a GitHub/BitBucket issue.


### Requirements

These apply to real life, as well.

* Must apply "good programming style" learned in class
    * Optimize for readability
* Bonus points for:
    * Creativity (as long as requirements are fulfilled)

## Teaming Tips

* Agree on an editor and environment that you're comfortable with
* The person who's less experienced/comfortable should have more keyboard time
* Switch who's "driving" regularly
* Make sure to save the code and send it to others on the team

## Evaluation

* Class Participation – 15%: students are expected to read all
   material covered in a week and come to class prepared to take
   part in the classroom discussions. Responding to other student
   questions (or issues in BitBucket) counts as classroom participation.
	
* Assignments - 40%: Each assignment will involve writing (or modifying a template of)
   a small Python program.

* Project - 45%: one original project done in a group of 2 or 3
   students. The project will explore one or more of the themes covered
   in the course that students find particularly compelling.  The
   group needs to submit a project proposal (2 pages IEEE format)
   approximately 1.5 months before the end of term.  The proposal
   should provide a brief motivation of the project, detailed
   discussion of the data that will be obtained or used in the project,
   along with a time-line of milestones, and expected outcome.

## Other considerations

As a programmer you will never write anything from scratch, but will
reuse code, frameworks, or ideas.  You are encouraged to
learn from the work of your peers. However, if you don't try to do
it yourself, you will not learn. [Deliberate practice][deliberate-practice]
(activities designed for the sole purpose of effectively improving
specific aspects of an individual's performance) is the only way to
reach perfection.

Please respect the terms of use and/or license of any code you find,
and if you re-implement or duplicate an algorithm or code from
elsewhere, credit the original source with an inline comment.

## Resources
### Materials

This class assumes you are confident with this material, but in case you need a brush-up...

* [Python for beginners](https://www.python.org/about/gettingstarted/)
  and [Python Dictionaries](http://pythonprogramminglanguage.com/dictionary/)

#### Other

* [Mining the Social Web, 2nd Edition](http://shop.oreilly.com/product/0636920030195.do)

##### Databases

* [A MongoDB Schema Analyzer](https://github.com/variety/variety). One JavaScript file that you run with the mongo shell command on a database collection and it attempts to come up with a generalized schema of the datastore. It was also written about on the official MongoDB blog.

##### R and data analysis
* Modern Applied Statistics with S (4th Edition) by William
  N. Venables, Brian D. Ripley. ISBN0387954570
* [Advanced Data Analysis from an Elementary Point of View by Cosma Rohilla Shalizi](http://www.stat.cmu.edu/~cshalizi/ADAfaEPoV/) 
* [R](https://www.coursera.org/learn/r-programming) 
* [Code School](http://www.codeschool.com/courses/try-r)
* [Quick-R](http://www.statmethods.net)

##### Tutorials written as ipython-notebooks
* [python-data-cleaning](http://nbviewer.ipython.org/github/ResearchComputing/Meetup-Fall-2013/blob/master/python/lecture_21_pandas_processing.ipynb)
* [python tutorial for engineers](http://nbviewer.ipython.org/gist/rpmuller/5920182)

* [how to display online notebooks (on, e.g., BitBucket)](https://bitbucket.org/lmondy/bitbucket_nbviewer/overview)

#### BitBucket

* [Learn how to use git with BB](https://confluence.atlassian.com/bitbucket/tutorial-learn-git-with-bitbucket-cloud-759857287.html)
* [What are pull-requests?](https://confluence.atlassian.com/bitbucket/tutorial-learn-about-pull-requests-in-bitbucket-cloud-774243385.html)
* [Issues](https://confluence.atlassian.com/bitbucket/use-the-issue-tracker-221449750.html)

#### Similar for GitHub

* [Official GitHub Help](https://help.github.com/)
* [pull-request](https://help.github.com/articles/creating-a-pull-request)
* [GitHub Issues](https://guides.github.com/features/issues/)
* [Recommended resources](https://help.github.com/articles/what-are-other-good-resources-for-learning-git-and-github)
* [create-repo](https://help.github.com/articles/create-a-repo)
* [forking](https://guides.github.com/activities/forking/)


#### Other links
* [deliberate-practice](http://www.psy.fsu.edu/faculty/ericsson/ericsson.exp.perf.html)
